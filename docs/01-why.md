# Why bundle the front-end?

Most applications starts out simple enough. However, over time, as your application grows in size, complexity, and scope, it becomes harder to manage.

As a result, your application can become difficult to reason about or affect at scale. The size of your application can bloat, performance can suffer, and customers get unhappy.

[WebPack][1] is a tool that aims to help you build the smallest, most efficient and effective version of your application. It achieves this through a variety of clever code analysis and transformation techniques. WebPack is also incredibly configurable and extensible, so you can adapt and change the compilation of your application as you need.

When it comes to the Atlassian Ecosystem, it has not been readily apparent how to take advantage of WebPack to manage your add-on's front-end code.

This tutorial aims to demonstrate how to integrate WebPack in to your add-on's build-time compilation, so you save time defining and building your add-on's front-end.


[1]: https://webpack.js.org/
