# Server Add-Ons for Front-End Developers Tutorial
## Bundling the UI of a Server Add-On

This tutorial demonstrates building a discrete front-end application bundle using [WebPack 4][201], and making it available in an Atlassian add-on via the [atlassian-webresource-webpack-plugin][202].

The codebase aims to demonstrate *how* you can compile your front-end code, *not the technologies to do so*. The front-end technology choices made here can be substituted for your own -- e.g., [TypeScript][105] instead of [ES2015][101], [NPM][107] instead of [Yarn][108], and so on.


## Getting started 

To build or run this add-on, you'll need to install Java and the Atlassian SDK. You can learn how to [set up the Atlassian SDK and build a project][2] on [developer.atlassian.com][1].

### Building the add-on

Once you have the Atlassian SDK installed and working, the only things you should have to do are: 

1. Open a terminal window or command prompt
2. Navigate to the checkout directory for this repository
2. Call `atlas-mvn package`

### See it in action

Once the add-on is built -- or indeed, to build the add-on and then run it -- you can call `atlas-run`.


## Tutorial

By completing this tutorial, you will know how to:

* Hook in to the build process of your add-on to change how your front-end code is prepared for production.
* Bundle your front-end code using WebPack.
* Push your bundled add-on code in to an Atlassian product.
* Remove the need to write `<web-resource>` definitions for your bundled add-on code.

You can read more on [the rationale for this tutorial][901] in the docs.

<<To be continued...>>



[1]: https://developer.atlassian.com
[2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project
[101]: https://github.com/lukehoban/es6features
[102]: https://en.wikipedia.org/wiki/ECMAScript#5th_Edition
[103]: https://babeljs.io/
[105]: https://www.typescriptlang.org/
[107]: https://docs.npmjs.com/getting-started/what-is-npm
[108]: https://yarnpkg.com/en/docs/getting-started
[201]: https://medium.com/webpack/webpack-3-official-release-15fd2dd8f07b
[202]: https://bitbucket.org/atlassianlabs/atlassian-webresource-webpack-plugin
[901]: docs/01-why.md
