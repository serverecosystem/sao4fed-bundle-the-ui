// this is needed to make webpack's code-split work in browsers that don't have native Promise.
// it is not valid AMD outside of almond.js, but that is okay; webpack will convert it to something useful to us.
require('vendor/es6-promise');

console.log('the glossary feature will load asynchronously');
require(['../glossary-of-terms'], function (module) {
    console.log('the glossary was lazy-loaded via wepack!', module);
});
