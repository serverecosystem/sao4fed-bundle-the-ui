/**
 * TODO: Should be custom element, or inline dialog.
 * TODO: Should get created by (and instance owned by) the term element itself.
 */
define('conf-glossary/feature/term-dialog/term-dialog', ['require'], function(require) {
    const Meta = AJS.Meta;
    const glossary = require('conf-glossary/feature/glossary/glossary');
    const _ = require('conf-glossary/lib/underscore');
    const $ = require('jquery');
    const Templates = require('./term-dialog.soy');
    require('./term-dialog.css');

    const $noTerm = $("<div></div>");
    const space = Meta.get('space-key');

    function getTermByName(name) {
        return glossary.findByTerm(name);
    }

    function createTermPopup(term) {
        const id = term.domId();
        const grouped = _.groupBy(term.definitions(), dfn => dfn.scope || "_");
        const data = _.clone(term.attributes);
        data.id = id;
        data.definitions = {
            here: grouped[space],
            global: grouped["_"]
        };

        let $el = $(document.getElementById(id));

        const html = Templates.popupContents(data).content;
        if (!$el.length) {
            $el = $(Templates.popupElement(data).content).html(html);
            $el.bind("mouseenter", e => { onEnterPopup($el); });
            $el.bind("mouseleave", e => { onLeavePopup($el); });
        } else {
            $el.html(html);
        }
        $el.appendTo(document.body);
        return $el;
    }

    function getTermPopup(termName) {
        const term = getTermByName(termName);
        if (!term) return $noTerm;
        term.popup = createTermPopup(term);
        return term.popup;
    }

    function onEnterPopup(popup) {
        const $popup = (popup.jquery) ? popup : $(popup);
        clearTimeout($popup.data("closing"));
        $popup.data("closing", null);
    }

    function onLeavePopup(popup) {
        const $popup = (popup.jquery) ? popup : $(popup);
        clearTimeout($popup.data("closing"));
        $popup.data("closing", setTimeout(() => { hidePopup($popup) }, 1000));
    }

    function hidePopup($popup) {
        if ($popup && $popup.data("closing")) {
            $popup.hide("slow");
        }
    }

    function showTermPopup(element, e) {
        const $el = $(element);
        const termName = $el.attr("title");
        const $popup = getTermPopup(termName);
        $popup.hide();
        $popup.css({
            "position": "fixed",
            "top": e.clientY,
            "left": e.clientX
        });
        $popup.show();
        onEnterPopup($popup);
    }

    function closeTermPopup(element) {
        const $el = $(element);
        const termName = $el.attr("title");
        const $popup = getTermPopup(termName);
        onLeavePopup($popup);
    }

    function hideAllPopupsRightNow() {
        $(".glossary-term-hover").each(function() {
            const $popup = $(this);
            clearTimeout($popup.data("closing"));
            $popup.hide();
        });
    }


    return {
        showTermPopup,
        closeTermPopup,
        hideAll: hideAllPopupsRightNow
    }
});
