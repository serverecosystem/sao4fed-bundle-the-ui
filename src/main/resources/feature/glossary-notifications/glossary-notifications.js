define('conf-glossary/feature/glossary-notifications/glossary-notifications', [
    'conf-glossary/feature/glossary/glossary',
    'conf-glossary/hack/format',
    'aui/flag',
], function(glossary, format, flag) {

    glossary.on('sync', (...args) => {
        console.log('glossary was synced', args);
    });

    glossary.on('add', model => {
        console.log('term added', model);
    });

    glossary.on('persisted', response => {
        console.log('persisted', response);
        const f = flag({
            type: 'success',
            title: format.I18n.getText('page.notification.term-saved.title'),
            body: format.I18n.getText('page.notification.term-saved.body.html', '<conf-term>', '</conf-term>'),
            persistent: false,
        });
        setTimeout(f.close, 5000);
    });
});
