const path = require('path');
const { DefinePlugin } = require("webpack");
const DirectoryNamedWebpackPlugin = require("directory-named-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const WrmPlugin = require("atlassian-webresource-webpack-plugin");

const PLUGIN_KEY = "com.atlassian.confluence.plugins.glossary-of-terms";

const MVN_OUTPUT_DIR = path.join(__dirname, 'target', 'classes');
const FRONTEND_SRC_DIR = path.join(__dirname, 'src', 'main', 'resources');
const NODE_MODULES_DIR = path.join(__dirname, 'node_modules');

const providedDependencies = require(path.resolve(FRONTEND_SRC_DIR, "wrm-dependencies-conf.js"));

module.exports = {
    mode: 'development',
    context: FRONTEND_SRC_DIR,
    entry: {
        'glossary/init': './page/webpack/init-app.js',
        'glossary-of-terms': './page/glossary-of-terms.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/env']
                    }
                }
            },
            {
                test: /\.(?:css)$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                ]
            },
            {
                test: /\.soy$/i,
                loader: 'soy-loader'
            },
        ],
    },

    plugins: [
        new MiniCssExtractPlugin(),
        new DefinePlugin({
            // outputs a literal string with the plugin key value in it
            PLUGIN_KEY: `"${PLUGIN_KEY}"`,
            // disables debug output in soy templates
            'goog.DEBUG': false,
        }),
        new WrmPlugin({
            pluginKey: PLUGIN_KEY,
            contextMap: {
                'glossary/init': ['page','blogpost']
            },
            providedDependencies: providedDependencies,
            xmlDescriptors: path.resolve(MVN_OUTPUT_DIR, "META-INF", "plugin-descriptors", "wr-webpack-bundles.xml")
        }),
    ],
    resolve: {
        alias: {
            'conf-glossary': FRONTEND_SRC_DIR,
            jquery$: path.join(FRONTEND_SRC_DIR, 'lib', 'jquery.js'),
            'vendor/es6-promise': path.join(NODE_MODULES_DIR, 'es6-promise', 'dist', 'es6-promise.auto.js'),
            'vendor/idb': path.join(NODE_MODULES_DIR, 'idb', 'lib', 'idb.js'),
        },
        modules: [
            path.resolve(FRONTEND_SRC_DIR),
            path.resolve(NODE_MODULES_DIR)
        ],
        plugins: [
            new DirectoryNamedWebpackPlugin()
        ]
    },
    output: {
        filename: 'bundle.[name].js',
        path: path.resolve(MVN_OUTPUT_DIR)
    }
};
